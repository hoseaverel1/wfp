<?php

use Illuminate\Database\Seeder;

class SupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<3;$i++){
            DB::table('suppliers')->insert([
                'nama_produk' => Str::random(10),
                'alamat_produk' => Str::random(10),
                'produk_id' => 1
            ]);
        }
    }
}
