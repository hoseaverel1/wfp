<?php

use Illuminate\Database\Seeder;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataku =[
            ['nama_kategori' => 'Kue Ulang Tahun'],
            ['nama_kategori' => 'Donut'],
            ['nama_kategori' => 'Roti Daging'],
            ['anam_kategori' => 'Roti Selai'],
            ['nama_kategori' => 'Cupcake']
        ];
        DB::table('kategoris')->insert($dataku);
    }
}
