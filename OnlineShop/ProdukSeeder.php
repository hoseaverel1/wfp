<?php

use Illuminate\Database\Seeder;

class ProdukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<3;$i++){
            DB::table('produks')->insert([
                'nama_produk' => Str::random(10),
                'stok_produk' => rand(1, 15),
                'harga_jual_produk' => rand(30000, 70000),
                'kategori_id' => 1,
                'harga_produksi_produk' => rand(10000,40000)
            ]);
        }
    }
}
