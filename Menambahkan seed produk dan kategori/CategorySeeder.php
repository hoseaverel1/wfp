<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataku =[
            ['category_name' => 'Kecantikan'],
            ['category_name' => 'Alat Rumah Tangga'],
            ['category_name' => 'Bahan Mentah'],
            ['category_name' => 'Bumbu Dapur'],
            ['category_name' => 'permainan']
        ];
        DB::table('categorys')->insert($dataku);
    }
}
