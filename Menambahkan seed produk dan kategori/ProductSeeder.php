<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<3;$i++){
            DB::table('products')->insert([
                'product_name' => Str::random(10),
                'product_price' => rand(10000, 50000),
                'category_id' => 1
            ]);
        }
    }
}
